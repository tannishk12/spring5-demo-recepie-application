package com.example.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by tannishs on 1/28/18.
 */
@Controller
public class IndexController {

    @RequestMapping("/index")
    public String index() {
        System.out.println("Index page call");
        return "index";
    }
}
